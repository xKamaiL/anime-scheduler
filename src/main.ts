import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import APP_CONFIG from './config/configuration'

async function bootstrap() {
  const app = await NestFactory.create(AppModule)
  await app.listen(APP_CONFIG.APP_PORT)
}
bootstrap()
