export enum AnimeStatus {
  ENDED = 'จบแล้ว',
  SHOWING = 'ยังไม่จบ',
}
