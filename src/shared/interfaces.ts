import { AnimeStatus } from './enums'

export interface IAnime {
  name: string
  part: string
  src: string
  status: AnimeStatus
  link: string
}

export interface ICovidDate {
  date: Date
  new_case: number
  new_recovered: number
  new_hospitalized: number
  deaths: number
  total: number
  total_recovered: number
  total_hospitalized: number
  total_deaths: number
}
