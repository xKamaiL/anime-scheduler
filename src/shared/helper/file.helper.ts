import { Storage } from '@google-cloud/storage'
import * as request from 'request'
import { BadRequestException } from '@nestjs/common'
import APP_CONFIG from '../../config/configuration'

export async function uploadCanvas(
  name: string,
  buffer: Buffer,
): Promise<void> {
  try {
    const storage = new Storage()
    return storage
      .bucket('anime-scheduler')
      .file(name)
      .save(buffer, {
        metadata: {
          'Cache-Control': 'no-cache',
        },
      })
  } catch (e) {
    throw new BadRequestException(e)
  }
}

export async function uploadFile(url: string): Promise<string> {
  try {
    const storage = new Storage()
    const file = await storage
      .bucket('anime-scheduler')
      .file(url.split('/').reverse()[0], {})

    await request
      .defaults({
        proxy: `http://${APP_CONFIG.HTTP_PROXY_HOST}:${APP_CONFIG.HTTP_PROXY_PORT}`,
      })
      .get(url)
      .pipe(file.createWriteStream())
    return 'https://storage.googleapis.com/anime-scheduler/' + file.name
  } catch (e) {
    throw new BadRequestException(e)
  }
}
