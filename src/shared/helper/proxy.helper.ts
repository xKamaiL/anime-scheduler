import * as tunnel from 'tunnel'

import APP_CONFIG from '../../config/configuration'

const getProxyConfig = (): any => {
  const config: any = {
    proxy: {
      host: APP_CONFIG.HTTP_PROXY_HOST,
      port: APP_CONFIG.HTTP_PROXY_PORT,
    },
  }
  if (APP_CONFIG.USE_HTTP_PROXY) {
    const tunnelingAgent = tunnel.httpsOverHttp(config)
    return {
      httpAgent: tunnelingAgent,
      httpsAgent: tunnelingAgent,
    }
  } else {
    return {}
  }
}

export default getProxyConfig
