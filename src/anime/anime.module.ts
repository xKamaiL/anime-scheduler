import { TypeOrmModule } from '@nestjs/typeorm'
import { AnimeController } from './anime.controller'
import { Module } from '@nestjs/common'
import { AnimeService } from './anime.service'
import { Anime } from 'src/entities/anime.entity'

@Module({
  imports: [TypeOrmModule.forFeature([Anime])],
  controllers: [AnimeController],
  providers: [AnimeService],
})
export class AnimeModule {}
