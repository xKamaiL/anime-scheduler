import { WEBHOOK_ANIME_SUGOI } from './../shared/webhooks'
import { BadRequestException, Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { IAnime } from 'src/shared/interfaces'
import { Anime } from 'src/entities/anime.entity'
import { Repository } from 'typeorm'
import { MessageBuilder, Webhook } from 'webhook-discord'
import { uploadFile } from 'src/shared/helper/file.helper'
import { ANIME_SUGOI } from '../shared/constants'
import axios from 'axios'
import getProxyConfig from '../shared/helper/proxy.helper'
import * as jsdom from 'jsdom'

const { JSDOM } = jsdom

@Injectable()
export class AnimeService {
  constructor(
    @InjectRepository(Anime)
    private readonly animeRepository: Repository<Anime>,
  ) {}

  async fetchAnimeSuigoi(): Promise<IAnime[]> {
    try {
      const { data } = await axios.get(ANIME_SUGOI, getProxyConfig())
      const { window } = new JSDOM(data)

      return [
        ...window.document.getElementsByClassName('col-xs-6 col-sm-4 col-md-3'),
      ]
        .splice(0, 8)
        .map((item) => {
          const tagA = item.getElementsByTagName('a')[0]
          const titles = tagA.title.split('ตอนที่ ')
          const img = item.getElementsByClassName('lazy')[0]
          const status = item.getElementsByClassName('label-ago')[0].innerHTML
          return {
            link: tagA.href,
            name: tagA.title,
            part: titles[1].split(' ')[0],
            src: img.getAttribute('data-src'),
            status,
          } as IAnime
        })
    } catch (err) {
      throw new BadRequestException(err)
    }
  }

  async upsertAnime(animes: IAnime[]): Promise<IAnime[]> {
    const newAnimes = []
    for (let i = 0; i < animes.length; i++) {
      try {
        await this.animeRepository.findOneOrFail({
          name: animes[i].name,
        })
      } catch (e) {
        newAnimes.push(animes[i])

        await this.animeRepository.save(animes[i])
      }
    }
    return newAnimes
  }

  async publish(animes: IAnime[]): Promise<any> {
    const Hook = new Webhook(WEBHOOK_ANIME_SUGOI)
    for (let i = 0; i < animes.length; i++) {
      const anime = animes[i]
      const image = await uploadFile(anime.src)
      const msg = new MessageBuilder()
        .setName('Anime-Sugoi')
        .setText(anime.link)
        .setColor('#aabbcc')
        .setTitle(anime.name.split(' ตอนที่')[0])
        .setImage(image)
        .setURL(anime.link)
        .addField('ตอนที่', anime.part, true)
        .addField('สถานะ', anime.status, true)
        .setTime()
        .setAvatar('https://storage.googleapis.com/anime-scheduler/icon.png')
      await Hook.send(msg)
    }
    return animes
  }
}
