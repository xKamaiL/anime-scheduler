import { AnimeService } from './anime.service'
import { Controller, Get } from '@nestjs/common'

@Controller('anime')
export class AnimeController {
  constructor(private readonly animeService: AnimeService) {}

  @Get('/anime-sugoi')
  async getNewAnimeFromAnimeSuigoi() {
    const animes = await this.animeService.fetchAnimeSuigoi()
    const newEpisodes = await this.animeService.upsertAnime(animes)
    const publish = await this.animeService.publish(newEpisodes.reverse())
    return { animes, newEpisodes, publish }
  }
}
