import { ICovidDate } from './../shared/interfaces'
import { Injectable, Res } from '@nestjs/common'
import axios from 'axios'
import * as dayjs from 'dayjs'
import APP_CONFIG from '../config/configuration'
import { uploadCanvas } from '../shared/helper/file.helper'

const { ChartJSNodeCanvas } = require('chartjs-node-canvas')

@Injectable({
  scope: 2,
})
export class CovidService {
  async getTimeline(): Promise<ICovidDate[]> {
    const { data } = await axios.get(
      'https://covid19.th-stat.com/api/open/timeline',
    )
    return data.Data.map((item) => ({
      date: dayjs(item.Date).toDate(),
      new_case: item.NewConfirmed,
      new_recovered: item.NewRecovered,
      new_hospitalized: item.NewHospitalized,
      deaths: item.NewDeaths,
      total: item.Confirmed,
      total_recovered: item.Recovered,
      total_hospitalized: item.Hospitalized,
      total_deaths: item.Deaths,
    })).slice(Math.max(data.Data.length - 7, 1))
  }

  async convertToChart(data: ICovidDate[], @Res() res) {
    const width = 400 * 2 //px
    const height = 300 * 2 //px
    const chartJSNodeCanvas = new ChartJSNodeCanvas({
      width,
      height,
      plugins: {
        modern: [require('chartjs-plugin-datalabels')],
      },
      chartCallback: (ChartJS) => {
        ChartJS.defaults.global.animation = false
        ChartJS.defaults.global.responsive = false
        ChartJS.defaults.global.defaultFontColor = '#FFFFFF'
        ChartJS.defaults.global.defaultFontSize = 22
        ChartJS.defaults.global.defaultFontStyle = 'bold'
        ChartJS.defaults.global.defaultFontFamily = 'Roboto'
      },
    })
    chartJSNodeCanvas.registerFont(
      APP_CONFIG.ROOT_PATH + '/Roboto/Roboto-Bold.ttf',
      {
        family: 'Roboto',
      },
    )
    const configuration = {
      type: 'bar',
      data: {
        labels: data.map((s) => dayjs(s.date).format('dddd')),
        datasets: [
          {
            label: 'New confirmed cases of COVID-19.',
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: '#822659',
            data: data.map((s) => s.new_case),
          },
        ],
      },
      options: {
        plugins: {
          datalabels: {
            align: 'start',
            anchor: 'start',
            color: 'white',
            font: {
              style: 'bold',
              family: 'Roboto',
            },
            formatter: Math.round,
          },
        },
        layout: {
          padding: {
            top: 24,
            right: 16,
            bottom: 0,
            left: 8,
          },
        },
        scales: {
          xAxes: [
            {
              gridLines: {
                color: '#ffedfe',
              },
            },
          ],
          yAxes: [
            {
              gridLines: {
                color: '#ffedfe',
              },
            },
          ],
        },
      },
    }
    // const stream = chartJSNodeCanvas.renderToStream(configuration)
    const dataUrl = await chartJSNodeCanvas.renderToDataURL(configuration)

    const img = Buffer.from(
      dataUrl.replace(/^data:image\/png;base64,/, ''),
      'base64',
    )
    const name = dayjs().unix()

    await uploadCanvas(`covid/${name}.png`, img)
    res.json({
      name: `https://storage.googleapis.com/anime-scheduler/covid/${name}.png`,
    })
  }
}
