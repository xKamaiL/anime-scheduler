import { CovidService } from './covid.service'
import { Controller, Get, Res } from '@nestjs/common'

@Controller('covid')
export class CovidController {
  constructor(private readonly service: CovidService) {}
  @Get('/timeline')
  async fetchCovidTimeline(@Res() res): Promise<any> {
    const result = await this.service.getTimeline()
    return this.service.convertToChart(result, res)
  }
}
