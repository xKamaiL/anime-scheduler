import { CovidController } from './covid.controller'
import { CovidService } from './covid.service'
import { Module } from '@nestjs/common'

@Module({
  imports: [],
  controllers: [CovidController],
  providers: [CovidService],
})
export class CovidModule {}
