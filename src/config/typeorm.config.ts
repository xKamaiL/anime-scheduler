import { TypeOrmModuleOptions } from '@nestjs/typeorm'
import APP_CONFIG from './configuration'

const typeOrmConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  host: APP_CONFIG.DB_HOST,
  port: APP_CONFIG.DB_PORT,
  username: APP_CONFIG.DB_USERNAME,
  password: APP_CONFIG.DB_PASSWORD,
  database: APP_CONFIG.DB_DATABASE,
  entities: [__dirname + '/../entities/*.entity.{js,ts}'],
  dropSchema: false,
  migrations: [__dirname + '/../**/migration/*.{js,ts}'],
  synchronize:
    process.env.NODE_ENV === 'production' ? false : APP_CONFIG.DB_SYNCHRONIZE,
  cli: {
    migrationsDir: 'src/migration',
  },
  logging: false,
}

export default typeOrmConfig
