import * as dotEnv from 'dotenv'

dotEnv.config()

interface IConfiguration {
  NODE_ENV: string
  APP_PORT: number
  DB_LOGGING: boolean

  DB_HOST: string
  DB_PORT: number
  DB_USERNAME: string
  DB_PASSWORD: string
  DB_DATABASE: string
  DB_SYNCHRONIZE: boolean
  HTTP_PROXY_HOST: string
  HTTP_PROXY_PORT: string
  USE_HTTP_PROXY: boolean
  ROOT_PATH: string
}

const APP_CONFIG: IConfiguration = {
  NODE_ENV: process.env.NODE_ENV || 'development',
  APP_PORT: parseInt(process.env.APP_PORT, 10) || 8080,
  DB_LOGGING: process.env.DB_LOGGING === 'true',

  DB_HOST: process.env.DB_HOST,
  DB_PORT: Number(process.env.DB_PORT),
  DB_USERNAME: process.env.DB_USERNAME,
  DB_PASSWORD: process.env.DB_PASSWORD,
  DB_DATABASE: process.env.DB_DATABASE,
  DB_SYNCHRONIZE: process.env.DB_SYNCHRONIZE === 'true',
  HTTP_PROXY_HOST: 'xkamail.moxga.com',
  HTTP_PROXY_PORT: '34567',
  USE_HTTP_PROXY: true,
  ROOT_PATH: process.cwd(),
}

export default APP_CONFIG
