import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm'

@Entity()
export class Anime {
  @PrimaryGeneratedColumn()
  id: number
  @Column()
  link: string
  @Column()
  name: string
  @Column()
  part: string
  @Column()
  src: string
  @Column()
  status: string

  @CreateDateColumn({ name: 'created_at' })
  public createdAt: Date

  @UpdateDateColumn({ name: 'updated_at' })
  public updatedAt: Date
}
